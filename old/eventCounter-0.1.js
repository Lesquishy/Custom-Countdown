var date = data["date"];
var done = false;

$(document).ready(function() {
   $("#countdown").html("");
   //PreVar
   var typeDay = "tdays";
   var monthDisplay = "";
   var dayDisplay = "";
   var hoursDisplay = "";
   var minutesDisplay = "";
   var secondsDisplay = "";
   var monthText = "Months";
   var dayText = "Days";
   var hourText = "Hours";
   var minutesText = "Minutes";
   var secondsText = "Seconds";
   var spaces = " ";


   //Just if settings aren't filled out entirely
   if(settings["counters"] == null){
     settings["counters"] = {};
   }
   if(settings["counters"]["days"] == null){
     settings["counters"]["days"] = "true";
   }
   if(settings["counters"]["hours"] == null){
     settings["counters"]["hours"] = "true";
   }
   if(settings["counters"]["minutes"] == null){
     settings["counters"]["minutes"] = "true";
   }
   if(settings["counters"]["seconds"] == null){
     settings["counters"]["seconds"] = "true";
   }

   if(settings["words"] == "letter"){
     monthText = "Mon";
     dayText = "D";
     hourText = "H";
     minutesText = "M";
     secondsText = "S";
   } else if (settings["words"] == "none"){
     monthText = "";
     dayText = "";
     hourText = "";
     minutesText = "";
     secondsText = "";
   }

   if(settings["spaces"] == "false"){
     spaces = "";
   }

   //What text to display
   if(settings["counters"]["months"] == "true"){
     monthDisplay = "<span id='allMonth'><span id='months'>00</span><span id='monthText'>" + spaces + monthText + "</span></span>";
     typeDay = "days";
     if(settings["counters"]["seconds"] == "true" || settings["counters"]["minutes"] == "true" || settings["counters"]["hours"] == "true" || settings["counters"]["days"] == "true"){
       monthDisplay += ", "
     }
   }
   if(settings["counters"]["days"] == "true"){
     dayDisplay = "<span id='" + typeDay + "'>00</span>" + spaces  + dayText;
     if(settings["counters"]["seconds"] == "true" || settings["counters"]["minutes"] == "true" || settings["counters"]["hours"] == "true"){
       dayDisplay += ", "
     }
   }
   if(settings["counters"]["hours"] == "true"){
     hoursDisplay = "<span id='hours'>00</span>" + spaces  + hourText;
     if(settings["counters"]["seconds"] == "true" || settings["counters"]["minutes"] == "true"){
       hoursDisplay += ", "
     }
   }
   if(settings["counters"]["minutes"] == "true"){
     minutesDisplay = "<span id='mins'>00</span>" + spaces  + minutesText;
     if(settings["counters"]["seconds"] == "true"){
       minutesDisplay += ", "
     }
   }
   if(settings["counters"]["seconds"] == "true"){
     secondsDisplay = "<span id='secs'>00</span>" + spaces + secondsText + "</div>";
   }
   //Displaying what needs to be displayed
   $("#countdown").html("<div>" + monthDisplay + dayDisplay + hoursDisplay + minutesDisplay + secondsDisplay);

  //start displaying
  window.setInterval('time(' + date + ')', 1000);
});

function time(year, month, day, hour, minutes, seconds) {
  var then = new Date(year, month - 1, day, hour, minutes, seconds, 0);
  var now = new Date();
  var currentDate = now.getDate();
  var thenDate = then.getDate();

  var delta = Math.floor(Math.abs(then - now) / 1000);
  var delt = Math.floor(Math.abs(then - now));
  var Cminutes = 1000 * 60;
  var Chours = Cminutes * 60;
  var Cdays = Chours * 24;
  var Cmonths = Math.floor(Cdays * 30.5);
  var Cyears = Cdays * 365;


  //Calculation
  var years = Math.floor(delt / Cyears);
  delt -= years * Cyears;
  var months = Math.floor(delt / Cmonths % 12);
  delt -= Math.floor(months * Cmonths);
  var days = delt / Cdays % 31;
  delt -= Math.floor(days * Cdays);


  //Calculation
  var tdays = Math.floor(delta / 86400);
  delta -= tdays * 86400;
  var hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;
  var mins = Math.floor(delta / 60) % 60;
  delta -= mins * 60;
  var secs = Math.round(delta % 60);
  tsecs = parseInt(tdays) * 86400 + parseInt(hours) * 3600 + parseInt(mins) * 60 + secs;


  //Total Time
  thour = parseInt(tdays) * 24 + parseInt(hours);
  tmins = parseInt(hours) * 60 + 1440 * parseInt(tdays) + mins;


  //Making all the numbers positive
  mins = Math.abs(mins);
  hours = Math.abs(hours);
  secs = Math.abs(secs);
  tdays = Math.abs(tdays);
  thour = Math.abs(thour);
  tmins = Math.abs(tmins);
  tsecs = Math.abs(tsecs);


  //Rounding
  months = Math.floor(months);
  days = Math.floor(days);


  //Adding a 0 infront of its less than 9
  if(settings["twoDigits"] == "true"){
    if (tdays <= 9) {
      tdays = '0' + tdays;
    }
    if (hours <= 9) {
      hours = '0' + hours;
    }
    if (mins <= 9) {
      mins = '0' + mins;
    }
    if (secs <= 9) {
      secs = '0' + secs;
    }
    if (months <= 9) {
      months = '0' + months;
    }
    if (days <= 9) {
      days = '0' + days;
    }
  }


  //Display
  if($("#months").text() != months){
    $('#months').text(months);
  }
  if($("#days").text() != days){
    $('#days').text(days);
  }
  if (tdays != $('#tdays').html()) {
    $('#tdays').text(tdays);
  }
  if (hours != $('#hours').html()) {
    $('#hours').text(hours);
  }
  if (mins != $('#mins').html()) {
    $('#mins').text(mins);
  }
  $('#secs').text(secs);


  //Hide options if 0
  if($("#months").text() == "00" || $("#months").text() == "0"){
    $("#allMonth").hide();
  }
  if($("#days").text() == "00" || $("#months").text() == "0"){
    $("#dayss, #days").hide();
  }

  //Triggering First Load
  if (done === false) {
    $("body").trigger("time");
    done = true;
  }
}
