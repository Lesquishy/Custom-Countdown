##### Check the demo out [here](http://merugby.comli.com/custom)
# Countdown ALPHA

[![Version](https://img.shields.io/badge/Version-0.27-00c12d.svg?style=flat-square)]()
[![Status](https://img.shields.io/badge/Development-Stopped-a11d00.svg?style=flat-square)]()
[![Language (JS)](https://img.shields.io/badge/Language-Javascript-blue.svg?style=flat-square)](https://javascript.com)
[![License (MIT)](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)](http://opensource.org/licenses/MIT)

### About
After making my [Countdown Site](https://gitlab.com/Lesquishy/Countdown). I decided to make it easier for other people to impliment a countdown.

#### Features
 - Auto hide numbers on '0'. This means when months or days are on 0 they will be hidden so you can have a cleaner and smaller countdown as time goes on
 - Ability to toggle the individual counters
 - Able to turn commas on and off depending on your need
 - Have the output to a variable or directly to the screen.
 - Once loaded you can have a trigger event.
 - Custom outputs

## Setting Variables

### Info
Between Variable you will need commas.

### Data
 - Date: The date variable needs to contain the date you'd like to count from / to. It is in the format of `Year, Months, Days, Hours, Minutes, Seconds`.

    For instance it if you wanted to count from / to 7th of January 2017 at 8:30pm it would look like

    ```javascript
    data = {
        "date":"2017, 01, 07, 20, 30, 0"
    }
    ```
<br>
### Settings
| <b>Var Name</b> | <b>Parameters</b> | <b>Use</b> |
| ---- | -------- | -------- |
| counters | Array   | *Information on Counters in in the next section `Counters`* |
| twoDigits | True / False  | To allow Single digits or to use a double digit. ( 4 or 04 ) Default its `True`.<ul><li>`True` it will use double digits </li><li>`False` Will display a sigle didget for anything < that 10</li></ul> |
| words | Initial, Letters, None | To change what words are displayed for identifying what each number is. Default its `True`.<ul><li>`Initial` will show the full word. (' 12 Days, 20 Hours').</li><li> `Letters` will show the letters ('12D, 20H').</li><li> `None` wont display anything other than the numbers them self (in 0.1 it will also show commas)</li></ul> |
| spaces | True / False | To allow spaces between the numbers / letters. Default its `True`.<ul><li>`True` will allow spaces.</li><li> `False` Will remove the spaces</li></ul> |
| commas | True / False | Allows commas between the numbers to be toggled on and off. Default its `True`. This will overwrite the seporator variable<ul><li>`True` Will keep the commas on. </li><li>`False` Will turn off commas</li></ul> |
| seporator | VAR | To allow custom seporators between the numbers |
| refreshRate | INT | The amount of time between countdown refreshing. Default is 1 second. 1 is equal to 1 second |
| doneTrigger | VAR | what event you want triggered after it has finished loading the first time. |
| display | True / False | Display any text on screen. Default is true. If display is false the countdown div is not needed <ul><li>`True` The countdown output will be displayed on screen</li><li>`False` There will be now visable output</li></ul> |
| output | variable / display | Where the output goes. Default its display. `variable` will output everything to a variable with the name `counterOut` |
<br>
### Counters
The counters variable controls what counters are shown. All default is `TRUE`

| <b>Var name</b> | <b>Parameters</b> | <b>Use</b> |
| ----- | ----- | ----- |
| months | True / False|<ul><li>`True` It will turn months display on and change days to the month day (from `59 days` to `1 month 29 days`)</li><li> `False` It will hide the month and change the Days back to default (From `1 month 29 days` To `59 days`)</li></ul>|
| days | True / False | <ul><li>`True` It will turn days</li><li>`False` It will hide the days</li></ul> |
| hours | True / False | <ul><li>`True` It will turn days</li><li>`False` It will hide the hours</li></ul> |
| minutes | True / False | <ul><li>`True` It will turn days</li><li>`False` It will hide the minutes</li></ul> |
| seconds | True / False | <ul><li>`True` It will turn days</li><li>`False` It will hide the seconds</li></ul> |

