data = {
  "date":"2016, 10, 07, 8, 30, 0"
},
settings = {
  "counters":{
    "months":"true",
    "seconds":"true"
  },
  "twoDigits":"true",
  "words":"letters",
  "spaces":"true",
  "commas":"true",
  "seporator":"-",
  "refreshRate":"1",
  "doneTrigger":"countDone",
  "display":"true",
  "output":"display"
}
